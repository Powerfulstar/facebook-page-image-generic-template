﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FacebookPageImage.Resources;
using FacebookPageImage.ViewModel;
using GalaSoft.MvvmLight.Messaging;

namespace FacebookPageImage
{
    public partial class MainPage : PhoneApplicationPage
    {
    
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            Messenger.Default.Register<Uri>(this, "Navigate", (uri) => NavigationService.Navigate(uri));
           
            //BuildLocalizedApplicationBar();
        }

        private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

            MessageBox.Show("NO IDEA WHY IMAGE IS NOT LOADING !!!   : \n");
        }

        //private void AlbumLongListSelectorItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        //{            
        //    Datum model = (Datum)((FrameworkElement)sender).DataContext;

        //    NavigationService.Navigate(new Uri(@"/AlbumPhotos.xaml?name=" + model.name + "&id=" + model.id, UriKind.Relative));
            
        //}

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}