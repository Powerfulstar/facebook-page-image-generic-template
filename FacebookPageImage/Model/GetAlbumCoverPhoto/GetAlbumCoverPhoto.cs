﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookPageImage.Model.GetAlbumCoverPhoto
{
    public class From
    {
        public string category { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Image : ViewModelBase
    {
        public int height { get; set; }
       // public string source { get; set; }
        #region Source
        /// <summary>
        /// The <see cref="source" /> property's name.
        /// </summary>
        public const string sourcePropertyName = "source";

        private string _source = string.Empty;

        /// <summary>
        /// Sets and gets the source property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string source
        {
            get
            {
                return _source;
            }

            set
            {
                if (_source == value)
                {
                    return;
                }

                RaisePropertyChanging(sourcePropertyName);
                _source = value;
                RaisePropertyChanged(sourcePropertyName);
            }
        }
        #endregion
        public int width { get; set; }
    }

    public class From2
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class MessageTag
    {
        public string id { get; set; }
        public int length { get; set; }
        public string name { get; set; }
        public int offset { get; set; }
        public string type { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public bool can_remove { get; set; }
        public string created_time { get; set; }
        public From2 from { get; set; }
        public int like_count { get; set; }
        public string message { get; set; }
        public bool user_likes { get; set; }
        public List<MessageTag> message_tags { get; set; }
    }

    public class Cursors
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class Paging
    {
        public string next { get; set; }
        public Cursors cursors { get; set; }
    }

    public class Comments
    {
        public List<Datum> data { get; set; }
        public Paging paging { get; set; }
    }

    public class Datum2
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Cursors2
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class Paging2
    {
        public string next { get; set; }
        public Cursors2 cursors { get; set; }
    }

    public class Likes
    {
        public List<Datum2> data { get; set; }
        public Paging2 paging { get; set; }
    }

    public class GetAlbumCoverPhoto
    {
        public string id { get; set; }
        public string created_time { get; set; }
        public From from { get; set; }
        public int height { get; set; }
        public string icon { get; set; }
        public List<Image> images { get; set; }
        public string link { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
        public string source { get; set; }
        public string updated_time { get; set; }
        public int width { get; set; }
        public Comments comments { get; set; }
        public Likes likes { get; set; }
    }
     
}
