﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookPageImage
{
    public class From
    {
        public string category { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Datum2
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Cursors
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class Paging
    {
        public string next { get; set; }
        public Cursors cursors { get; set; }
    }

    public class Likes
    {
        public List<Datum2> data { get; set; }
        public Paging paging { get; set; }
    }

    public class From2
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Datum3
    {
        public string id { get; set; }
        public bool can_remove { get; set; }
        public string created_time { get; set; }
        public From2 from { get; set; }
        public int like_count { get; set; }
        public string message { get; set; }
        public bool user_likes { get; set; }
    }

    public class Cursors2
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class Paging2
    {
        public Cursors2 cursors { get; set; }
    }

    public class Comments
    {
        public List<Datum3> data { get; set; }
        public Paging2 paging { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public From from { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public string cover_photo { get; set; }
        public int count { get; set; }
        public string type { get; set; }
        public string created_time { get; set; }
        public string updated_time { get; set; }
        public bool can_upload { get; set; }
        public Likes likes { get; set; }
        public Comments comments { get; set; }
    }

    public class Cursors3
    {
        public string after { get; set; }
        public string before { get; set; }
    }

    public class Paging3
    {
        public Cursors3 cursors { get; set; }
    }

    public class AlbumModel
    {
        public ObservableCollection<Datum> data { get; set; }
        public Paging3 paging { get; set; }
    }
}
