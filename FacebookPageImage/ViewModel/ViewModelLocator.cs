/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:FacebookPageImage"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using Microsoft.Practices.ServiceLocation;
using System.Windows;


namespace FacebookPageImage.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

          
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<AlbumPhotosViewModel>();

             
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }
        public AlbumPhotosViewModel AlbumPhotos
        {
            get
            {
                //ServiceLocator.Current.GetInstance<AlbumPhotosViewModel>().GetID(Main.ID); //eida ki chilo? :)

                return ServiceLocator.Current.GetInstance<AlbumPhotosViewModel>();
            }
        }
        
        public static void Cleanup()
        {
          var VM =   ServiceLocator.Current.GetInstance<AlbumPhotosViewModel>();
          VM = null;
        }
    }
}