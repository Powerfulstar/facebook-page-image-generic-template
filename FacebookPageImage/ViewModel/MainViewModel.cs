﻿using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using FacebookPageImage.Model;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
 
using FacebookPageImage.Model.GetAlbumCoverPhoto;
using System.Windows.Media;
using GalaSoft.MvvmLight.Command;
using System.Windows.Navigation;
using System;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
namespace FacebookPageImage.ViewModel
{
 
    public class MainViewModel : ViewModelBase
    {
        public const string graphURL = @"http://graph.facebook.com/";
        public string ID { get; set; }
        
        #region Datum Property
        /// <summary>
        /// The <see cref="Datum" /> property's name.
        /// </summary>
        public const string DatumPropertyName = "Data";

        private ObservableCollection<Datum> _datum = new ObservableCollection<Datum>();

        /// <summary>
        /// Sets and gets the Datum property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Datum> Data
        {
            get
            {
                return _datum;
            }

            set
            {
                if (_datum == value)
                {
                    return;
                }

                RaisePropertyChanging(DatumPropertyName);
                _datum = value;
                RaisePropertyChanged(DatumPropertyName);
            }
        }
        #endregion
        public RelayCommand<object> NavigationToSecondPage { get; set; }
        public MainViewModel()
        {
            LoadAsyncAlbumGetOperation();
            NavigationToSecondPage = new RelayCommand<object>((param) => GotoNextPage(param));
        }
        public void GotoNextPage(object param)
        {
            string data = param as string;
           // Constants.TemporarySelectedID = data;  // এই যে এইখানে Constants.TemporarySelectedID এর ভ্যালু হিসেবে অ্যাল্বাম আইডী সেট করা হয়



            (App.Current.Resources["Locator"] as ViewModelLocator).AlbumPhotos.ID = data;

            Uri uri = new Uri("/AlbumPhotos.xaml", UriKind.Relative);
                Messenger.Default.Send<Uri>(uri, "Navigate");   // আর এইখানে পেইজ ন্যাভিগেশন হয়
             
           

        }
        public async void LoadAsyncAlbumGetOperation()
        {
            Data = (await LoadAlbum()).data;
        }
        private async Task<AlbumModel> LoadAlbum()
        {
            AlbumModel TempAlbum = new AlbumModel();
            HttpClient AlbumClient = new HttpClient();
            string DownloadedAlbumJson = await AlbumClient.GetStringAsync(@"http://graph.facebook.com/moja.losss/albums/");
            TempAlbum = JsonConvert.DeserializeObject<AlbumModel>(DownloadedAlbumJson);
            foreach (var item in TempAlbum.data)
            {
                string source = await LoadCoverPhoto(item.cover_photo);
                item.cover_photo = source;
            }
            return TempAlbum;
        }
        private async Task<string> LoadCoverPhoto(string coverPhotoID)
        {
            GetAlbumCoverPhoto tempGetCoverPhoto = new GetAlbumCoverPhoto();
            HttpClient getCoverPhotoClient = new HttpClient();
            string getCoverPhotoJson = await getCoverPhotoClient.GetStringAsync(graphURL+coverPhotoID);
            tempGetCoverPhoto = JsonConvert.DeserializeObject<GetAlbumCoverPhoto>(getCoverPhotoJson);
            return  tempGetCoverPhoto.images[0].source;
        }
    }
}