﻿using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using FacebookPageImage.Model;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using GalaSoft.MvvmLight.Messaging;
namespace FacebookPageImage.ViewModel
{
 
    public class AlbumPhotosViewModel : ViewModelBase
    {

        /// <summary>
        /// The <see cref="ID" /> property's name.
        /// </summary>
        public const string IDPropertyName = "ID";

        private string _ID = string.Empty;

        /// <summary>
        /// Sets and gets the ID property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ID
        {
            get
            {
                return _ID;
            }

            set
            {
                if (_ID == value)
                {
                    return;
                }

                
               

                RaisePropertyChanging(IDPropertyName);
                _ID = value;
                RaisePropertyChanged(IDPropertyName);

                LoadData(value);
            }
        }

        #region AlbumPhotosCollection Property
        /// <summary>
        /// The <see cref="AlbumPhotosCollection" /> property's name.
        /// </summary>
        public const string AlbumPhotosCollectionPropertyName = "AlbumPhotosCollection";

        private AlbumPhotosModel _albumPhotosModel = new AlbumPhotosModel();

        /// <summary>
        /// Sets and gets the AlbumPhotosCollection property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public AlbumPhotosModel AlbumPhotosCollection
        {
            get
            {
                return _albumPhotosModel;
            }

            set{
                        // প্রতিবার পেইজে নেভিগেশনে ভিউমডেলএর এই প্রোপার্টি এইখানে লোড হয় :D 
                if (_albumPhotosModel == value)
                {
                    return;
                }

                RaisePropertyChanging(AlbumPhotosCollectionPropertyName);
                _albumPhotosModel = value;
                RaisePropertyChanged(AlbumPhotosCollectionPropertyName);
            }
        }
        #endregion
      
        public AlbumPhotosViewModel()
        {

            //Messenger.Default.Register<string>(this,"ID", GetID);

           // LoadData(Constants.TemporarySelectedID);        // Constants.TemporarySelectedID একটা স্ট্যাটিক ভ্যারিয়েবল, যাকে কিনা মেইনপেইজ থেকে নেভিগেশনের সময় প্রতিবার ভ্যালু চেঞ্জ করে দেয়া হয়
                                                            // আর সে ওই অনুযায়ী ডাটা লোডডাটা এর প্যারামিটার হিসেবে ব্যবহৃত হয়
        }
        
        public void GetID(string id)
        {
            ID = id;
        }
        void LoadData(string ID)
        {
            LoadDataAsync(ID);
        }
        private async void LoadDataAsync(string ID)
        {
            //AlbumPhotosCollection = await LoadAlbumPhotos();
            ObservableCollection<AlbumPhotosModel> tempAlbumPhotosModel = new ObservableCollection<AlbumPhotosModel>();

            HttpClient client = new HttpClient();
            string URI = Constants.GarphFacebook + ID + Constants.photos;
            string Json = await client.GetStringAsync(URI);
            AlbumPhotosCollection = JsonConvert.DeserializeObject<AlbumPhotosModel>(Json);
            //return tempAlbumPhotosModel;
        }
        //public async Task<ObservableCollection<AlbumPhotosModel>> LoadAlbumPhotos(string id)
        //{
        //    ObservableCollection<AlbumPhotosModel> tempAlbumPhotosModel = new ObservableCollection<AlbumPhotosModel>();

        //    HttpClient client = new HttpClient();
        //    string Json = await client.GetStringAsync(@"http://graph.facebook.com/"+id+@"/Photos");
        //    AlbumPhotosCollection = JsonConvert.DeserializeObject<ObservableCollection<AlbumPhotosModel>>(Json);
        //    return tempAlbumPhotosModel;
        //}
    }
}