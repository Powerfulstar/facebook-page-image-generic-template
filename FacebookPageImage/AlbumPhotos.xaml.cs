﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FacebookPageImage.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;

namespace FacebookPageImage.Model
{
    public partial class AlbumPhotos : PhoneApplicationPage
    {
        string ID = "";
        public AlbumPhotos()
        {
            
            InitializeComponent();
            
        }

       
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {

            //you can use OnBackKeyPress buddy! I commented the code behind


            if (e.NavigationMode == NavigationMode.Back)
            {
                ServiceLocator.Current.GetInstance<AlbumPhotosViewModel>().AlbumPhotosCollection.data.Clear();
                //ViewModelLocator.Cleanup();
                base.OnNavigatedFrom(e);
            }
        }

        //protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        //{
        //    base.OnBackKeyPress(e);
        //}
      
    }
}